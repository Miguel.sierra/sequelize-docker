//Server imports
const express = require('express');
const logger = require('morgan')
const bodyparser = require('body-parser')
const cors = require('cors')
const helmet = require('helmet')
const dotenv = require('dotenv').config()
const passport = require('./auth/passport').passport

//Server env vars
const port = process.env.PORT || 3000


//Server routes
const user = require('./routes/user')
const auth = require('./routes/auth')


//Database
const db = require('./models/index')

//Initial connection database
db.sequelize
  .authenticate()
  .then(function(err) {
    console.log('Connection has been established successfully.');
  })
  .catch(function (err) {
    console.log('Unable to connect to the database:', err);
  });

//Server initial configuration
let app = express()
app.use(logger('dev'))
app.use(cors());
app.use(helmet())
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended: true}))
app.use(passport.initialize());


//Server Routes
app.use('/api/',user)
app.use('/api/',auth)


//just for check
app.get('/', (req,res) => {
  res.send('All working')
})

//Initialize server
app.listen(port, () => {
    console.log(`Our server is running on port ${port}`);
});
  