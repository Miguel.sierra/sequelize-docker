FROM node:alpine

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY package.json ./
RUN npm install
RUN npm install pm2 -g
RUN npm install -g sequelize-cli
RUN npm install -g nodemon
RUN npm test

COPY . .


EXPOSE 3000



