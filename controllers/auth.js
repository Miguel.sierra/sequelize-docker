const User = require('../models/index').User
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken');
const jwtOptions = require('../auth/passport').jwtOptions

login = async (req, res, next) => { 
    try {
        const { name, password } = req.body;
        if (name && password) {
          // we get the user with the name and save the resolved promise returned
          let user = await User.findOne({ name });
          if (!user) {
            res.status(401).json({ msg: 'No such user found', user });
          }
          bcrypt.compare(password, user.password, (err,pool)=>{
              if (err) {
                console.log(error)
                res.status(500).send('Error checking user')
              }
              if (pool) {
                 // from now on we’ll identify the user by the id and the id is
           // the only personalized value that goes into our token
                 let payload = { id: user.id };
                 let token = jwt.sign(payload, jwtOptions.secretOrKey);
                 res.json({ msg: 'ok', token: token });
               } else {
                 res.status(401).json({ msg: 'Password is incorrect' });
               }
          })
        }
    } catch (error) {
        console.log(error)
        res.status(500).send('Error login user')
    }
}

module.exports.login = login