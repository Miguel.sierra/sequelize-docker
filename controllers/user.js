// create some helper functions to work on the database
const User = require('../models/index').User
const bcrypt = require('bcryptjs')

const createUser = async (req,res) => {
    try {
        let password = req.body.password
        let name = req.body.name
        bcrypt.genSalt(10, async function(err,salt){
            bcrypt.hash(password,salt,async function(err,hash){
                if (err) res.status(500).send('Error crypting')
                let user = await User.create({ name, password:hash, role:'regular user' });
                res.json(user)
            })
        }) 
    } catch (error) {
        console.log(error)
        res.status(500).send('Error creating user')
    }
};

const infoUser = async (req,res) => {
    try {
        console.log(await req.user)
        res.json(await req.user)
    } catch (error) {
        console.log(error)
        res.status(500).send('Error getting user')
    }
}

module.exports = {
    createUser: createUser,
    infoUser: infoUser
};