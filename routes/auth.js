//Server import
const express = require('express')
const auth = require('../controllers/auth')
const passport = require('passport');
const router = express.Router()

router.post('/login', auth.login)


module.exports = router