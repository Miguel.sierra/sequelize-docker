//Server import
const express = require('express')
const user = require('../controllers/user')
const passport = require('passport');
const router = express.Router()

router.post('/register', user.createUser)
router.get('/info', passport.authenticate('jwt', {session:true}), user.infoUser)

module.exports = router
